# Package

version       = "0.2.0"
author        = "Zrean Tofiq"
description   = "Calculate the next semver version given the git log and previous version"
license       = "MIT"
srcDir        = "src"
binDir        = "bin"
bin           = @["conventional_semver"]
installExt    = @["nim"]


# Dependencies

requires "nim >= 1.6.2"
requires "semver >= 1.1.1"
requires "simpleparseopt >= 1.1.1"

import std/strformat
task release, "Create the next tag and push it":
  let (lastTag, exitCode) = gorgeEx "git describe --tags --abbrev=0"
  if exitCode != 0:
    echo "Could not get the last tag"
  else:
    let (nextVer, exitCode) = gorgeEx fmt"git log {lastTag}..HEAD | docker run -i --rm registry.gitlab.com/simplyz/conventional_semver:latest {lastTag}"
    if exitCode != 0:
      echo "Could not get the next version"
    else:
      echo nextVer
      exec fmt"git tag {nextVer}"
      exec "git push --tags"
