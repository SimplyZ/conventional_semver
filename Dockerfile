FROM nimlang/nim:1.6.2-alpine as build

COPY . .

RUN nimble -y build --passL:-static -d:release

#####################################

FROM scratch

COPY --from=build bin/conventional_semver .

ENTRYPOINT [ "./conventional_semver" ]
