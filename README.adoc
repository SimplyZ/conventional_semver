= Conventional semver

WARNING: This tool is still under development and the API may change without notice. Please pin to specific releases to avoid issues.

A piece of code speaks more than a thousand words. So here's the tl;dr.
```bash
$ PREVIOUS_TAG=$(git describe --tags --abbrev=0) # for example 1.1.1
$ git log $PREVIOUS_TAG..HEAD | conventional_semver $PREVIOUS_TAG
1.1.2
```

This program will take a list of commit messages from stdin and a tag as an argument. It will then output the next semver version number, based on the commit log using the conventional commit specification.

If no commits are found that match the conventional commit specification, the patch version number will be bumped.

In the spirit of keeping things simple, conventional_semver will not do any git operations.
It aims to do one thing and to do it well: Calculate the next version.

== Installation

This software is written in Nim and you can either choose to install it using their package manager, called Nimble, or you can use the <<usage,docker image we provide.>>

It is also provided as a nim library and can be called programmatically.

=== Install using Nimble

**Prerequisites**

* Nim
* Nimble

Run `nimble install conventionalsemver`

That's it!

== Usage [[usage]]

=== Using the library

```nim
import conventional_semver

echo v"1.1.2".nextVersion("fix: some simple message")

#outputs "1.1.3"
```

=== Using the binary

Once installed though nimble you can use the binary directly.

```bash
$ PREVIOUS_TAG=$(git describe --tags --abbrev=0) # for example 1.1.1
$ git log $PREVIOUS_TAG..HEAD | conventional_semver $PREVIOUS_TAG
1.1.2
```

Check out `conventional_semver -help` for more information about parameters.

=== Docker container

**Docker repo:** registry.gitlab.com/simplyz/conventional_semver

The software is provided as a Docker container.
You may use it as such:

```bash
$ PREVIOUS_TAG=$(git describe --tags --abbrev=0)
$ git log $PREVIOUS_TAG..HEAD | docker run -i --rm registry.gitlab.com/simplyz/conventional_semver:latest $PREVIOUS_TAG
```

Need version pinning? Use commit hashes as docker tags.