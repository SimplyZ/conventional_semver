import conventional_semver, unittest

check nextVersion(v "1.1.1", "") == v "1.1.2"

test "patch log messages":
    const patchLogs = ["""
        commit 323f8113c2ae604fd7bd042eed38cdef82495a41
        Author: John Doe <john.doe@gmail.com>
        Date:   Wed Dec 22 09:23:05 2021 +0100

            fix: calculate semver from commits
    """,
    """
        commit 323f8113c2ae604fd7bd042eed38cdef82495a41
        Author: John Doe <john.doe@gmail.com>
        Date:   Wed Dec 22 09:23:05 2021 +0100

            chore: calculate semver from commits
    """
    ]

    for log in patchLogs:
        check nextVersion(v "1.1.1", log) == v "1.1.2"



test "feature log messages":
    const featLogs = @["""
        commit 323f8113c2ae604fd7bd042eed38cdef82495a41
        Author: John Doe <john.doe@gmail.com>
        Date:   Wed Dec 22 09:23:05 2021 +0100

            feat: calculate semver from commits
    """,
    """
        commit 323f8113c2ae604fd7bd042eed38cdef82495a41
        Author: John Doe <john.doe@gmail.com>
        Date:   Wed Dec 22 09:23:05 2021 +0100

            feat(some-scope): calculate semver from commits
    """
    ]

    for log in featLogs:
        check nextVersion(v "1.1.1", log) == v "1.2.0"


test "breaking log messages":
    const breakingLogs = @["""
        commit 323f8113c2ae604fd7bd042eed38cdef82495a41
        Author: John Doe <john.doe@gmail.com>
        Date:   Wed Dec 22 09:23:05 2021 +0100

            feat: calculate semver from commits

            BREAKING CHANGE: we changed stuff
    """,
    """
        commit 323f8113c2ae604fd7bd042eed38cdef82495a41
        Author: John Doe <john.doe@gmail.com>
        Date:   Wed Dec 22 09:23:05 2021 +0100

            feat!: calculate semver from commits
    """
    ]

    for log in breakingLogs:
        check nextVersion(v "1.1.1", log) == v "2.0.0"
