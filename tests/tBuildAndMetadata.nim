import conventional_semver, unittest

test "Adds build number and metadata":
    check nextVersion(v "1.1.1", "", "123", "abc") == v "1.1.2-123+abc"
