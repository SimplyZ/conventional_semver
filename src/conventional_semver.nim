import strutils, sequtils, sugar, semver

export semver
using ver: Version

const
  breaking = ["!:", "BREAKING CHANGE:"]
  feature = ["feat:", "feat("]

func incMajor(ver): Version =
  newVersion ver.major.succ, 0, 0

func incMinor(ver): Version =
  newVersion ver.major, ver.minor.succ, 0

func incPatch(ver): Version =
  newVersion ver.major, ver.minor, ver.patch.succ

func nextVersion*(ver; log: string; build, meta: string = ""): Version =
  result = if breaking.any(str => str in log):
             ver.incMajor
           elif feature.any(str => str in log):
             ver.incMinor
           else:
             ver.incPatch
  result.build = build
  result.metadata = meta

when isMainModule:
  import os, simple_parseopt, std/[terminal, logging]

  var logger = newConsoleLogger()
  addHandler(logger)

  help_text("""
This tool will calculate the next semver version using commit messages.
It will follow the conventional commit specification to determine the next version.
It takes the output of 'git log' on stdin and prints the next version to stdout.
  """)

  let options = get_options:
    build: string {.info("Build label of the next version").}
    meta: string {.info("Metadata of the next version").}
    version: string {.info("The version to be bumped"), bare, need.}

  if stdin.isatty:
    error """No input found on stdin. You must provide the output 'git log' on stdin.
      Try running 'git log $PREVIOUS_TAG..HEAD | conventional_commit $PREVIOUS_TAG'"""
    quit 1

  let commit_messages = readAll stdin
  let ver = parseVersion options.version
  echo ver.nextVersion(commit_messages, options.build, options.meta)
